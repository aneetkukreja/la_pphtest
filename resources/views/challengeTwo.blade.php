
<form action="{{ route('challenge2') }}" method="post">

    @csrf
    <label for="rounds">Rounds 
        <input type="number" id="rounds" name="rounds" value="" required >
    </label>

    <label for="contestants">Contestants 
        <input type="number" id="contestants" name="contestants" value="{{ old('contestants') }}" required >
    </label>

    <button type="submit" name="submit">Submit</button>
    
</form>

@if(isset($result_tree) && count($result_tree) > 0)

    @php 
        $last_key = count($result_tree)-1;
    @endphp 

    @foreach($result_tree as $key => $groups)

        @if($key != $last_key)
            <h3>Round: {{ $key+1 }}</h3>
        @else
            <h3>Winner:</h3>
        @endif

        @foreach($groups as $group)

            @if(is_array($group))

                @foreach($group as $member)
                    {{ $users[$users_map[$member]]->name }} <br/> 
                @endforeach

            @else
                {{ $users[$users_map[$group]]->name }} <br/> 
            @endif

             <br /><br />

        @endforeach

    @endforeach
@endif