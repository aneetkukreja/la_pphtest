
@if(isset($result_tree) && count($result_tree) > 0)

    @php 
        $last_key = count($result_tree)-1;
    @endphp 

    @foreach($result_tree as $key => $pairs)

        @if($key != $last_key)
            <h3>Round: {{ $key+1 }}</h3>
        @else
            <h3>Winner:</h3>
        @endif

        @foreach($pairs as $pair)

            @if(isset($pair[1]))
                {{ $users[$users_map[$pair[0]]]->name }} <br/> {{ $users[$users_map[$pair[1]]]->name }}
            @else
                {{ $users[$users_map[$pair]]->name }}
            @endif

             <br /><br />

        @endforeach

    @endforeach
@endif