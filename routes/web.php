<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('challenge1', 'CompetitionController@challengeOne');
Route::get('challenge2', 'CompetitionController@challengeTwo');
Route::post('challenge2', 'CompetitionController@challengeTwo')->name('challenge2');