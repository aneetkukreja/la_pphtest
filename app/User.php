<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;

    public static function random($count)
    {
    	return User::inRandomOrder()->limit($count)->get();
    }

    public static function makePairs($users)
    {
    	$pairs = [];

    	for ($i=0; $i < count($users); $i+=2) { 
    		$pairs[] = [ $users[$i], $users[$i+1] ];
    	}

    	return $pairs;
    }

    public static function makeGroups($users, $group_size = 2)
    {
        $groups = [];

        for ($i=0, $j=0; $i < count($users); $i+=$group_size, $j++) { 
            for($k=0; $k<$group_size; $k++) {
                $groups[$j][$k] = $users[$i+$k];
            }
        }

        return $groups;
    }

    public static function printExit($data)
    {
        echo '<pre>';
        print_r($data);
        die;
    }

    public static function makeGroups2($users, $group_size = 2)
    {
        $groups = [];

        $j = 0;
        $l = 0;

        while(count($users) >= $group_size) {
            for($k=0; $k<$group_size; $k++) {
                $groups[$j][$k] = $users[$l];
                unset($users[$l]);
                $l++;
            }
            $j++;
        }

        if(count($users) > 0) {
            $i = 0;
            foreach($users as $user) {
                $groups[$i][] = $user;
                $i++;
            }
        }

        return $groups;
    }

    protected static function getGroupSize($rounds, $contestants)
    {
        return ($rounds > 2) ? ceil($contestants**(1/$rounds)) : floor($contestants/$rounds);
    }

    public static function map($users)
    {
    	$return = [];

    	foreach ($users as $key => $user) {
    		$return[$user->id] = $key;
    	}

    	return $return;
    }

}
