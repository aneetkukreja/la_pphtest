<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use InvalidArgumentException;


class CompetitionController extends Controller
{

    public function challengeOne()
    {
        $rounds = 3;
        $count = 8;

        $users = User::random($count);

        $user_ids = $users->pluck('id')->toArray();

        $users_map = User::map($users);

        $groups = User::makeGroups($user_ids);

        $result_tree = $this->makeCompetition($rounds, $groups);

        return view('challengeOne', [
            'users' => $users,
            'users_map' => $users_map,
            'result_tree' => $result_tree
        ]);
    }

    public function challengeTwo(Request $request)
    {
    	if($request->method() == 'POST') {

    		$rounds = $request->input('rounds');

    		$contestants = $request->input('contestants');

            $users = User::random($contestants);

            $user_ids = $users->pluck('id')->toArray();

            $users_map = User::map($users);

            $group_size = User::getGroupSize($rounds, $contestants);

            $groups = User::makeGroups2($user_ids, $group_size);

            $result_tree = $this->makeCompetition($rounds, $groups, $group_size);

            return view('challengeTwo', [
                'users' => $users,
                'users_map' => $users_map,
                'result_tree' => $result_tree
            ]);
    	}

    	return view('challengeTwo');
    }

    protected function makeCompetition($rounds, $groups, $group_size = 2)
    {
    	$tree = [];
    	$tree_i = 0;

    	while ($tree_i+1 <= $rounds) {

    		$return = $this->competitionLoop($rounds, $groups, $tree, $tree_i, $group_size);

    		$groups = $return['groups'];
    		$tree = $return['tree'];
    		$tree_i++;
    	}

    	$winner = $this->getWinner($groups);
    	$tree[$tree_i][] = $winner;

    	return $tree;
    }

    protected function competitionLoop($rounds, $groups, $tree, $tree_i, $group_size = 2)
    {
        for($i=0; $i<count($groups); $i++) {

            if($rounds >= $tree_i+1) {
                for($j=0; $j<count($groups[$i]); $j++) {
                    $tree[$tree_i][$i][$j] = $groups[$i][$j];
                }
                $result[] = $this->getWinner($tree[$tree_i][$i]);
            }
            else {

                $tree[$tree_i][] = $groups[$i][0];
                $tree[$tree_i][] = $groups[$i][1];
                $result[] = $groups[$i][0];
            }
        }

        if($rounds > $tree_i+1) {
            $group_size = User::getGroupSize(($rounds - $tree_i - 1), count($result));
            $result = User::makeGroups2($result, $group_size);
        }

    	return [
    		'groups' => $result,
    		'tree' => $tree
    	];
    }

    protected function getWinner($group)
    {
    	return $group[array_rand($group)];
    }

}
